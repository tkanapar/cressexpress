/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CressExpress_data_generation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.*;

/**
 *
 * @author lorainelab
 */
public class Experiment2Array {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String user = "ndahake";
            String pass = "itfungtopove1!";
            String URL = "jdbc:mysql://localhost:6963/coexpression2?autoReconnect=true";//forwarded port 6963 on local machine to db.transvar.org:3306 using putty
            //  String URL = "jdbc:mysql://db.transvar.org:3306/coexpression2?autoReconnect=true";
            

            String mappingfile = args[0];
            String DataFile = args[1];
            Connection conn = DriverManager.getConnection(URL, user, pass);


           

            FileReader fr = new FileReader(mappingfile);
            BufferedReader br = new BufferedReader(fr);

            FileWriter fw = new FileWriter(DataFile);
            BufferedWriter bw = new BufferedWriter(fw);


            String str = "";
            String selectSQL = "SELECT id FROM experiment WHERE name = ?";
            String selectSQL2 = "SELECT id FROM array WHERE name = ?";
            PreparedStatement st = conn.prepareStatement(selectSQL);
            PreparedStatement st2 = conn.prepareStatement(selectSQL2);
            ResultSet res, res2;

            while ((str = br.readLine()) != null) {


                String arr[] = str.split("\t");
                String GSE = arr[0];
                String GSM = arr[1];
                String GSE_ID = "";
                String GSM_ID = "";

                st.setString(1, GSE);
                res = st.executeQuery();
                while (res.next()) {
                    GSE_ID = res.getInt("id") + "";
                    // System.out.print(GSE_ID + "\t");
                }



                st2.setString(1, GSM);
                res2 = st2.executeQuery();
                while (res2.next()) {
                    GSM_ID = res2.getInt("id") + "";
                    //System.out.print(GSM_ID + "\t");
                }


                System.out.print(GSE_ID + "\t" + GSM_ID + "\n");


            }

            bw.flush();
            bw.close();
            conn.close();
            st.close();


        } catch (Exception e) {

            System.out.println(e.toString());
        } finally {
        }

    }
}
