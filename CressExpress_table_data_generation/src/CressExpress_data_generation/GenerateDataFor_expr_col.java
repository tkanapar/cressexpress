/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CressExpress_data_generation;

import com.google.common.base.Splitter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Iterator;

/**
 *
 * @author lorainelab
 */
public class GenerateDataFor_expr_col {

    public static void main(String[] args) throws IOException {

        String inputFile = args[0];
        String outputFile = args[1];
        FileWriter fw = new FileWriter(outputFile);
        BufferedWriter bw = new BufferedWriter(fw);

        FileReader fr = new FileReader(inputFile);
        BufferedReader br = new BufferedReader(fr);


        String line = null;
        StringBuilder lineToPrint = new StringBuilder();
        while ((line = br.readLine()) != null) {
            line = line.trim();
            lineToPrint = new StringBuilder();
            Iterable<String> result = Splitter.on(",").trimResults().split(line);
            String str = null;
            int element = 0;
            String arr_ID = null;
            Iterator<String> itr = result.iterator();

            try {
                while (itr.hasNext()) {

                    str = itr.next();
                    if (element == 0) {
                        //str is a array id
                        arr_ID = str;
                        lineToPrint.append(arr_ID);
                        element = 1;
                    } else {

                        float number = round(str, 2);
                        lineToPrint.append(",").append(number);
                    }

                }
            } catch (Exception e) {
                System.out.println(e.toString());
            }


            // System.out.println(line);

            int i = getArray_ID(arr_ID);
            if (i == -1) {
                System.out.println("Error occurred here");
            }
            bw.write("4" + "\t" + i + "\t" + arr_ID + "\t" + lineToPrint.toString() + "\n");

        }


        bw.flush();
        fw.flush();
        bw.close();
        fw.close();

        fr.close();
        br.close();

    }

    static int getArray_ID(String GSM) {
        int i = -1;

        try {

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String user = "ndahake";
            String pass = "itfungtopove1!";

            String URL = "jdbc:mysql://localhost:6963/coexpression2?autoReconnect=true";

            Connection conn = DriverManager.getConnection(URL, user, pass);



            String SQL = "SELECT id FROM coexpression2.array WHERE name = ";
            Statement stmnt = conn.createStatement();


            ResultSet res;


            res = stmnt.executeQuery(SQL + "'" + GSM + "'");

            while (res.next()) {
                i = res.getInt("id");

            }


            conn.close();
            stmnt.close();


        } catch (Exception e) {

            System.out.println(e.toString());
        }

        return i;

    }

    public static float round(String float_str, int decimalPlace) {
        BigDecimal bd = new BigDecimal(float_str);
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }
}
