/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CressExpress_data_generation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author nikhildahake
 */
public class Generate_experiment_table_data {

    static String user = "ndahake";
    static String pass = "itfungtopove1!";
    static String URL = "jdbc:mysql://localhost:6963/coexpression2?autoReconnect=true";//forwarded port 6963 on local machine to db.transvar.org:3306 using putty
   // static String URL = "jdbc:mysql://db.transvar.org:3306/coexpression2?autoReconnect=true"; // uncomment if running program on a computer on the UNC Charlotte network.
    static Connection conn;
    static Statement stmnt;
    static ResultSet result;

    public static void main(String args[]) throws Exception {

        Class.forName("com.mysql.jdbc.Driver").newInstance();

        conn = DriverManager.getConnection(URL, user, pass);

        String pathToXML_files = args[0];//Full Path to the directory where all GSE_XMLs are stored
        String writeFile = args[1];//Full path to the file where the mapping will be written

        FileWriter writer = new FileWriter(writeFile);

        BufferedWriter bw = new BufferedWriter(writer);
        int nextExperimentId = getLargestExperimentIDfromDB() + 1;
        File folder = new File(pathToXML_files);
        File[] listOfFiles = folder.listFiles();

        System.out.println("Please wait...");

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {

                String fileName = listOfFiles[i].getName();
                String extension = "";

                if (fileName != null && !fileName.trim().equals("")) {
                    int j = fileName.lastIndexOf('.');
                    if (j > 0) {
                        extension = fileName.substring(j + 1);
                    }
                }
                if (fileName.toUpperCase().startsWith("GSE") && extension.equalsIgnoreCase("xml")) {

                    int LastIndexOfPeriod = fileName.lastIndexOf(".");
                    String GSE_ID = fileName.substring(0, LastIndexOfPeriod);//stripping of xml

                    boolean entryExistsInTable = checkIf_GSE_exists_In_table(GSE_ID);

                    if (!entryExistsInTable) {
                        bw.write(nextExperimentId + "\t" + GSE_ID);
                        bw.write("\n");
                        nextExperimentId++;
                    }
                }
            }
        }


        bw.flush();
        bw.close();
        writer.close();
        stmnt.close();
        result.close();
        conn.close();

        System.out.println("Mappings have been written to: " + writeFile);

    }

    static int getLargestExperimentIDfromDB() throws Exception {
        String SQL = "SELECT Max(id) FROM coexpression2.experiment";
        stmnt = conn.createStatement();
        result = stmnt.executeQuery(SQL);

        int maxId = 0;
        while (result.next()) {
            maxId = result.getInt(1);
        }
        return maxId;
    }

    static boolean checkIf_GSE_exists_In_table(String GSE_ID) throws Exception {
        String SQL = "SELECT Count(*) FROM coexpression2.experiment where name =\"" + GSE_ID + "\" ";
        stmnt = conn.createStatement();
        result = stmnt.executeQuery(SQL);

        int noOfRows = 0;
        while (result.next()) {
            noOfRows = result.getInt(1);
        }
        if (noOfRows == 0) {
            return false;//entry does not exist in array table
        } else {
            return true; //entry exists in array table
        }
    }
}
