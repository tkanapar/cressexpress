/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CressExpress_data_generation;

import java.sql.DriverManager;
import java.sql.*;
import java.io.*;

/**
 *
 * @author lorainelab
 */
public class Experiment_attribute {

    public static void main(String[] args) {

        String URL = "jdbc:mysql://localhost:6963/coexpression2?autoReconnect=true";//forwarded port 6963 on local machine to db.transvar.org:3306 using putty
// static String URL = "jdbc:mysql://db.transvar.org:3306/coexpression2?autoReconnect=true"; // uncomment if running program on a computer on the UNC Charlotte network. 
        try {

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String user = "ndahake";
            String pass = "itfungtopove1!";

            String dataFile = args[0];//Full path to the file which was generated by the CreateDataFileForGSE.py
            String dataFileProcessed = args[1];// Full path the output file.
            Connection conn = DriverManager.getConnection(URL, user, pass);


            String xml_file_path = "";

            FileReader fr = new FileReader(xml_file_path + dataFile);
            BufferedReader br = new BufferedReader(fr);

            FileWriter fw = new FileWriter(xml_file_path + dataFileProcessed);
            BufferedWriter bw = new BufferedWriter(fw);

            System.out.println("Please wait...");
            String SQL = "SELECT id FROM experiment WHERE name = ";
            ResultSet res;
            Statement stmnt = conn.createStatement();
            String line;
            String finalLine = "";
            while ((line = br.readLine()) != null) {
                line = line.trim();
                String arr[] = line.split("\t");
                res = stmnt.executeQuery(SQL + "'" + arr[0] + "'");
                String id = " ";
                while (res.next()) {
                    id = res.getString("id");
                }
                arr[0] = id;
                finalLine = "";
                for (int i = 0; i < arr.length; i++) {
                    finalLine = finalLine + arr[i] + "\t";
                }

                finalLine = finalLine.trim();
                bw.write(finalLine + "\n");
            }
            bw.flush();
            fw.flush();
            bw.close();
            fw.close();


            System.out.println("Output written to: " + dataFileProcessed);
        } catch (Exception e) {
            System.out.println(e.toString());
        }

    }
}
