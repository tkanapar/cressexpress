/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab;

import com.google.common.base.Charsets;
import com.google.common.base.Splitter;
import com.google.common.io.CharStreams;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.Assert;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dcnorris
 */
public class CsvParseTest {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(CsvParseTest.class);
    private static final String EXPRESSION_DATA_FLAT_FILE = "/vagrant/env/data/exprRowData.txt";
    private final double[][] expressionData = new double[22_810][8_941];
    List<String> microarrayOrder;
    List<String> probesetNames;

    @Before
    public void testParseFile() throws Exception {
        try (Reader in = new FileReader(EXPRESSION_DATA_FLAT_FILE)) {
            int i = 0;
            for (CSVRecord record : CSVFormat.DEFAULT.parse(in)) {
                for (int j = 0; j < record.size(); j++) {
                    expressionData[i][j] = Double.parseDouble(record.get(j));
                }
                i++;
            }
        } catch (IOException ex) {
            
        }
        String microarrayOrderSource = CharStreams.toString(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("microarrayOrder.txt"), Charsets.UTF_8));
        String probesetNamesSource = CharStreams.toString(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("probesetNames.txt"), Charsets.UTF_8));

        microarrayOrder = Splitter.on(',').trimResults().omitEmptyStrings().splitToList(microarrayOrderSource);
        probesetNames = Splitter.on(',').trimResults().omitEmptyStrings().splitToList(probesetNamesSource);

    }

    @Test
    public void checkAllProbeSetsExist() {
        Assert.assertEquals(probesetNames.size(), 22_810);
        Assert.assertEquals(microarrayOrder.size(), 8_941);
        for (int i = 0; i < probesetNames.size(); i++) {
            for (int j = 0; j < microarrayOrder.size(); j++) {
                Assert.assertNotNull(expressionData[i][j]);
            }
        }
    }

}
