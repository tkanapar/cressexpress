/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab;

import java.io.IOException;
import javax.script.ScriptException;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author dcnorris
 */
public class RenjinAnalysisOld {

//    List<String> microarrayOrder;
//    List<String> microarraySelections;
//    List<String> probesetSelections;
//    double plcRsquaredThreshold = 0.36;
//    String expressionFlatFile = "/home/dcnorris/NetBeansProjects/rcressexpress/AllAgainstAll/cressExpressFlatFileDump/exprRowData.txt";

    @Before
    public void setup() throws IOException {
//        String microarrayOrderSource = CharStreams.toString(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("microarrayOrder.txt"), Charsets.UTF_8));
//        String microarraySelectionsSource = CharStreams.toString(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("microarraySelections.txt"), Charsets.UTF_8));
//        String probesetSelectionsSource = CharStreams.toString(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("probesetSelections.txt"), Charsets.UTF_8));
//        microarrayOrder = Splitter.on(',').trimResults().omitEmptyStrings().splitToList(microarrayOrderSource);
//        probesetSelections = Splitter.on(',').trimResults().omitEmptyStrings().splitToList(probesetSelectionsSource);
//        microarraySelections = Splitter.on(',').trimResults().omitEmptyStrings().splitToList(microarraySelectionsSource);
    }

    @Test
    public void correlationAnalysis() throws ScriptException, IOException {
//        ScriptEngineManager manager = new ScriptEngineManager();
//        ScriptEngine engine = manager.getEngineByName("Renjin");
//
//        File dbFile = new File("/home/dcnorris/expressionData");
//        DB db = DBMaker.newFileDB(dbFile).mmapFileEnable().transactionDisable().asyncWriteEnable().cacheHardRefEnable().make();
//        Map<String, Map<String, Double>> expressionData = db.getTreeMap("map");
//        if (expressionData.isEmpty()) {
//            Reader in = new FileReader(expressionFlatFile);
//            for (CSVRecord record : CSVFormat.DEFAULT.parse(in)) {
//                String probesetName = record.get(0);
//                Map<String, Double> probesetExprValues = Maps.newHashMapWithExpectedSize(22_810);
//                for (int i = 1; i < record.size(); i++) {
//                    probesetExprValues.put(microarrayOrder.get(i - 1), Double.parseDouble(record.get(i)));
//                }
//                expressionData.put(probesetName, probesetExprValues);
//                db.commit();
//            }
//        }
//        List<Double> results = new ArrayList<>(microarrayOrder.size() * microarraySelections.size());
//        for (String probeSet : expressionData.keySet()) {
//            double[] x = new double[microarraySelections.size()];
//            for (int i = 0; i < microarraySelections.size(); i++) {
//                String microarray = microarraySelections.get(i);
//                x[i] = expressionData.get(probeSet).get(microarray);
//            }
//            for (String probeSelection : probesetSelections) {
//                double[] y = new double[microarraySelections.size()];
//                for (int i = 0; i < microarraySelections.size(); i++) {
//                    String microarray = microarraySelections.get(i);
//                    y[i] = expressionData.get(probeSelection).get(microarray);
//                }
//                engine.put("x", x);
//                engine.put("y", y);
//                SEXP res = (SEXP) engine.eval("cor(x,y)");
//                results.add(res.asReal());
//            }
//
//        }
//        for (Double d : results) {
//            System.out.println(d);
//        }

    }

}
