/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab;

import com.google.common.base.Charsets;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.common.collect.TreeMultimap;
import com.google.common.io.CharStreams;
import com.google.common.io.FileWriteMode;
import com.google.common.io.Files;
import com.google.common.primitives.Doubles;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import static java.util.stream.Collectors.toList;
import javax.script.ScriptException;
import junit.framework.Assert;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.redisson.Redisson;
import org.redisson.core.RMap;

/**
 *
 * @author dcnorris
 */
public class RegressionAnalysisJava1 {

    List<String> microarrayOrder;
    List<String> probesetNames;
    List<String> microarraySelections;
    List<String> probesetSelections;

    double plcRsquaredThreshold = 0.6;
    String expressionFlatFile = "/opt/cressExpressEnv/local/env/data/exprRowData.txt";
    String resultInfoFlatFile = "ps_info.txt";
    Redisson redisson;

    double[][] expressionData = new double[22_810][9_780];
    RMap<String, Map<String, String>> resultInfoData;

    @Before
    public void setup() throws IOException {
        redisson = Redisson.create();
        String microarrayOrderSource = CharStreams.toString(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("microarrayOrder.txt"), Charsets.UTF_8));
        String probesetNamesSource = CharStreams.toString(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("probesetNames.txt"), Charsets.UTF_8));
        microarrayOrder = Splitter.on(',').trimResults().omitEmptyStrings().splitToList(microarrayOrderSource);
        probesetNames = Splitter.on(',').trimResults().omitEmptyStrings().splitToList(probesetNamesSource);

        Reader in = new FileReader(expressionFlatFile);
        int i = 0;
        for (CSVRecord record : CSVFormat.DEFAULT.parse(in)) {
            for (int j = 0; j < record.size(); j++) {
                expressionData[i][j] = Double.parseDouble(record.get(j));
            }
            i++;
        }

        resultInfoData = redisson.getMap("resultInfoData");
        if (resultInfoData.isEmpty()) {
            String source = CharStreams.toString(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream(resultInfoFlatFile), Charsets.UTF_8));
            Reader infoReader = new StringReader(source);
            int index = 0;
            for (CSVRecord record : CSVFormat.TDF.parse(infoReader)) {
                //skip header line
                if (index > 0) {
                    String probesetName = record.get(0);
                    Map<String, String> geneInfo = new HashMap<>();
                    geneInfo.put("Unique", record.get(1));
                    geneInfo.put("Redundant", record.get(2));
                    geneInfo.put("AGI", record.get(3));
                    geneInfo.put("Symbol", record.get(4));
                    geneInfo.put("Description", record.get(5));
                    resultInfoData.fastPut(probesetName, geneInfo);
                }
                index++;
            }

            //fill in any gaps with NA
            for (String probeset : probesetNames) {
                if (!resultInfoData.containsKey(probeset)) {
                    Map<String, String> geneInfo = new HashMap<>();
                    geneInfo.put("Unique", "NA");
                    geneInfo.put("Redundant", "NA");
                    geneInfo.put("AGI", "NA");
                    geneInfo.put("Symbol", "NA");
                    geneInfo.put("Description", "NA");
                    resultInfoData.fastPut(probeset, geneInfo);
                }
            }

        }
    }

    @Test
    public void correlationAnalysis() throws ScriptException, IOException {

        String microarraySelectionsSource = CharStreams.toString(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("microarraySelections.txt"), Charsets.UTF_8));
        String probesetSelectionsSource = CharStreams.toString(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("probesetSelections.txt"), Charsets.UTF_8));
        probesetSelections = Splitter.on(',').trimResults().omitEmptyStrings().splitToList(probesetSelectionsSource);
        microarraySelections = Splitter.on(',').trimResults().omitEmptyStrings().splitToList(microarraySelectionsSource);
        List<Integer> microarraySelectionIndexes = getMicroarrayIndexes(microarraySelections);

        BiFunction<List<Double>, List<Double>, Double> cor = (x, y) -> new PearsonsCorrelation().correlation(Doubles.toArray(x), Doubles.toArray(y));

        Function<Integer, List<Double>> probesetExprVals = probesetIndex
                -> microarraySelectionIndexes.stream().map(selectionIndex -> expressionData[probesetIndex][selectionIndex]).collect(toList());

        List<List<Double>> xVals = probesetSelections.stream().map(probeset -> probesetExprVals.apply(probesetNames.indexOf(probeset))).collect(toList());

        List<List<Double>> yVals = probesetNames.stream().map(probeset -> {

            return probesetExprVals.apply(probesetNames.indexOf(probeset));
        }).collect(toList());

        List<Double> results = xVals.stream().map(x -> yVals.stream().map(y -> cor.apply(x, y))).flatMap(s -> s.parallel()).collect(toList());

        List<List<Double>> splitByProbesetSelection = Lists.partition(results, probesetNames.size());
        Assert.assertEquals(splitByProbesetSelection.size(), probesetSelections.size());

        //write to file
        Map<String, List<Double>> corResultMap = new HashMap<>();
        for (int i = 0; i < splitByProbesetSelection.size(); i++) {
            corResultMap.put(probesetSelections.get(i), splitByProbesetSelection.get(i));
        }
//        Files.asCharSink(new File("/home/dcnorris/corResults.csv"), Charsets.UTF_8, FileWriteMode.APPEND).write(Joiner.on('\n').withKeyValueSeparator(",").join(corResultMap.entrySet()));

        //write result file
        //Remove old result files if exist
        new File("/opt/cressExpressEnv/local/env/data/results.tsv").delete();
        new File("/opt/cressExpressEnv/local/env/data/results-plc.tsv").delete();
        //write header

        String header = "ps" + "\t" + "Unique" + "\t" + "Redundant" + "\t" + "AGI" + "\t" + "Symbol" + "\t" + "Description" + "\t";
        for (String selection : probesetSelections) {
            header = header + "cor." + selection + "\t";
        }
        header = header + "\n";
        writeToResultFile(header);
        for (Map.Entry<String, Map<String, String>> entry : resultInfoData.entrySet()) {
            String ps = entry.getKey();
            writeToResultFile(ps + "\t");
            Map<String, String> infoMap = entry.getValue();
            writeToResultFile(infoMap.get("Unique") + "\t");
            writeToResultFile(infoMap.get("Redundant") + "\t");
            writeToResultFile(infoMap.get("AGI") + "\t");
            writeToResultFile(infoMap.get("Symbol") + "\t");
            writeToResultFile(infoMap.get("Description") + "\t");
            for (String selection : probesetSelections) {
                writeToResultFile(corResultMap.get(selection).get(probesetNames.indexOf(ps)) + "\t");
            }
            writeToResultFile("\n");
        }

        String plcHeader = "nc" + "\t" + "ps" + "\t" + "Unique" + "\t" + "Redundant" + "\t" + "AGI" + "\t" + "Symbol" + "\t" + "Description" + "\t";
        for (String selection : probesetSelections) {
            plcHeader = plcHeader + "cor." + selection + "\t";
        }
        plcHeader = plcHeader + "\n";
        writeToResultPLCFile(plcHeader);
        TreeMultimap<Integer, String> sortedResults = TreeMultimap.create(Ordering.natural().reverse(), Ordering.natural());
        for (Map.Entry<String, Map<String, String>> entry : resultInfoData.entrySet()) {
            String ps = entry.getKey();
            StringBuilder sb = new StringBuilder();
            sb.append(ps).append("\t");
            Map<String, String> infoMap = entry.getValue();
            sb.append(infoMap.get("Unique")).append("\t");
            sb.append(infoMap.get("Redundant")).append("\t");
            sb.append(infoMap.get("AGI")).append("\t");
            sb.append(infoMap.get("Symbol")).append("\t");
            sb.append(infoMap.get("Description")).append("\t");
            int nc = 0;

            for (String selection : probesetSelections) {
                Double corVal = corResultMap.get(selection).get(probesetNames.indexOf(ps));
                sb.append(corVal).append("\t");
                if (corVal > plcRsquaredThreshold) {
                    nc++;
                }
            }
            if (nc >= 2) {
                //buffer all writes to treeMap 
                sortedResults.put(nc, sb.toString());
            }

        }
        //write to file
        for (Map.Entry<Integer, String> entry : sortedResults.entries()) {
            writeToResultPLCFile(entry.getKey() + "\t");
            writeToResultPLCFile(entry.getValue());
            writeToResultPLCFile("\n");
        }

    }

    private List<Integer> getMicroarrayIndexes(List<String> microarraySelections) {
        List<Integer> indexList = new ArrayList<>();
        for (String s : microarraySelections) {
            indexList.add(microarrayOrder.indexOf(s));
        }
        return indexList;
    }

    private void writeToResultFile(String s) throws IOException {
        Files.asCharSink(new File("/opt/cressExpressEnv/local/env/data/results.tsv"), Charsets.UTF_8, FileWriteMode.APPEND).write(s);
    }

    private void writeToResultPLCFile(String s) throws IOException {
        Files.asCharSink(new File("/opt/cressExpressEnv/local/env/data/results-plc.tsv"), Charsets.UTF_8, FileWriteMode.APPEND).write(s);
    }

    @After
    public void close() {
        redisson.shutdown();
    }
}
