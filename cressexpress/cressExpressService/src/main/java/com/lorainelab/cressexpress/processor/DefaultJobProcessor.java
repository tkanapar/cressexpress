/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab.cressexpress.processor;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.common.collect.TreeMultimap;
import com.google.common.primitives.Doubles;
import com.lorainelab.cressexpress.model.Job;
import com.lorainelab.cressexpress.model.Microarray;
import com.lorainelab.cressexpress.reference.MicroarrayDetailReference;
import com.lorainelab.cressexpress.reference.ProbesetInfoReference;
import com.lorainelab.cressexpress.reference.ReleaseVersionReference;
import com.lorainelab.cressexpress.reference.ResultInfoReference;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.BiFunction;
import java.util.function.DoubleFunction;
import java.util.function.Function;
import static java.util.stream.Collectors.toList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.inject.Inject;
import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.Resource;
import javax.ejb.DependsOn;
import javax.ejb.Startup;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

/**
 *
 * @author dcnorris
 */
@Startup
@DependsOn({"ProbesetInfoReference", "ResultInfoReference", "ReleaseVersionReference"})
@Singleton
public class DefaultJobProcessor {

    private static final Logger logger = LoggerFactory.getLogger(DefaultJobProcessor.class);
    private static final String CRESSEXPRESS_EMAIL = "no-reply@cressexpress.org";
    private final double[][] expressionData = new double[22_810][8_941];

    //TODO create function to generate this file if it doesn't exist... 
    private static final String EXPRESSION_DATA_FLAT_FILE = "/vagrant/env/data/exprRowData.txt";

    @Inject
    private ProbesetInfoReference probesetInfoReference;

    @Inject
    private ResultInfoReference resultInfoReference;

    @Inject
    private ReleaseVersionReference releaseVersionReference;

    @Inject
    private MicroarrayDetailReference microarrayDetailReference;

    @Resource(mappedName = "java:jboss/mail/Default")
    private Session mailSession;

    private String[][] resultInfoData;
    private Set<String> probesetNames;
    private Map<String, Integer> probesetIndexReference;
    private List<Microarray> defaultReleaseMicroarrays;

    @PostConstruct
    private void setup() {
        initializeExpressionData();
        defaultReleaseMicroarrays = microarrayDetailReference.getDefaultReleaseMicroarrays();
        resultInfoData = resultInfoReference.getResultInfoData();
        probesetNames = probesetInfoReference.getProbesetNames();
        probesetIndexReference = probesetInfoReference.getProbesetIndexReference();
    }

    public void process(Job job) {
        try {
            //Retrieve probeset data from exprRow entities
            List<String> probesetSelections = Splitter.on(',').trimResults().omitEmptyStrings().splitToList(job.getProbesetSelections());
            List<String> microarraySelections = Splitter.on(',').trimResults().omitEmptyStrings().splitToList(job.getArraySelections());
            List<String> microarrayOrder = retrieveMicroarrayOrder(job);

            List<Integer> microarraySelectionIndexes = getMicroarrayIndexes(microarrayOrder, microarraySelections);
            List<Integer> probesetSelectionIndexes = probesetSelections.parallelStream().map(probeset -> probesetIndexReference.get(probeset)).collect(toList());

            logger.info("Starting correlation analysis");

            BiFunction<List<Double>, List<Double>, Double> cor = (x, y) -> new PearsonsCorrelation().correlation(Doubles.toArray(x), Doubles.toArray(y));

            Function<Integer, List<Double>> probesetExprVals = probesetIndex
                    -> microarraySelectionIndexes.parallelStream().map(selectionIndex -> {
                        return expressionData[probesetIndex][selectionIndex];
                    }).collect(toList());

            logger.info("Calculating xVals");
            List<List<Double>> xVals = probesetSelectionIndexes.parallelStream().map(index -> {
                return probesetExprVals.apply(index);

            }).collect(toList());
            logger.info("Calculating yVals");
            List<List<Double>> yVals = probesetNames.parallelStream().map(probeset -> {
                return probesetExprVals.apply(probesetIndexReference.get(probeset));
            }).collect(toList());

            logger.info("Calculating results");
            List<Double> results = xVals.parallelStream().map(x -> yVals.parallelStream().map(y -> cor.apply(x, y))).flatMap(s -> s.parallel()).collect(toList());

            DoubleFunction<Double> abs = input -> Math.abs(input);

            results = results.parallelStream().map(input -> abs.apply(input)).collect(toList());

            logger.info("Splitting results");
            List<List<Double>> splitByProbesetSelection = Lists.partition(results, probesetNames.size());
            logger.info("Results split");
            logger.info("Creating result map");
            Map<String, List<Double>> corResultMap = new TreeMap<>();
            for (int i = 0; i < splitByProbesetSelection.size(); i++) {
                corResultMap.put(probesetSelections.get(i), splitByProbesetSelection.get(i));
            }
            logger.info("Result map created");

            logger.info("Creating output files");
            generateOutputFiles(corResultMap, probesetSelections, job);
            logger.info("Output files created");
            //createResultArchive(job);
            emailResults(job);
            logger.info("Job" + job.getId() + " completed successfully");
        } catch (Throwable t) {
            logger.error("Error while processing job" + job.getId(), t);
        }

    }

    private void generateOutputFiles(Map<String, List<Double>> corResultMap, List<String> probesetSelections, Job job) throws IOException {
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("/vagrant/env/data/results/results-" + job.getId() + ".tsv", true)))) {
            logger.info("Creating standard output file");
            out.print("ps" + "\t" + "Unique" + "\t" + "Redundant" + "\t" + "AGI" + "\t" + "Symbol" + "\t" + "Description" + "\t");
            probesetSelections.stream().forEach(selection -> out.print("cor." + selection + "\t"));
            out.print("\n");
            for (String[] row : resultInfoData) {
                String ps = row[0];
                for (String value : row) {
                    out.print(value + "\t");
                }
                probesetSelections.stream().forEach(selection -> out.print(corResultMap.get(selection).get(probesetIndexReference.get(ps)) + "\t"));
                out.print("\n");
            }
            logger.info("Standard output file created");
        } catch (IOException ex) {
            logger.error("could not create result output file", ex);
        }
        generatePlcFile(corResultMap, probesetSelections, job);
    }

    private void generatePlcFile(Map<String, List<Double>> corResultMap, List<String> probesetSelections, Job job) {
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("/vagrant/env/data/results/results-plc-" + job.getId() + ".tsv", true)))) {
            logger.info("Creating plc output file");
            out.print("nc" + "\t" + "ps" + "\t" + "Unique" + "\t" + "Redundant" + "\t" + "AGI" + "\t" + "Symbol" + "\t" + "Description" + "\t");
            probesetSelections.stream().forEach(selection -> out.print("cor." + selection + "\t"));
            out.print("\n");
            TreeMultimap<Integer, String> sortedResults = TreeMultimap.create(Ordering.natural().reverse(), Ordering.natural());
            for (String[] row : resultInfoData) {
                String ps = row[0];
                StringBuilder sb = new StringBuilder();
                for (String value : row) {
                    sb.append(value).append("\t");
                }
                int nc = 0;
                for (String selection : probesetSelections) {
                    Double corVal = corResultMap.get(selection).get(probesetIndexReference.get(ps));
                    sb.append(corVal).append("\t");
                    if (corVal > job.getPlcRsquared()) {
                        nc++;
                    }
                }
                if (nc >= 2) {
                    //buffer all writes to treeMap 
                    sortedResults.put(nc, sb.toString());
                }
            }
            //write to file
            sortedResults.entries().stream().forEach(entry -> out.print(entry.getKey() + "\t" + entry.getValue() + "\n"));
            logger.info("plc output file created");
        } catch (IOException ex) {
            logger.error("could not create result output file", ex);
        }
    }

    private void createResultArchive(Job job) {
        try (FileOutputStream fos = new FileOutputStream("/vagrant/env/data/results/cressexpressResults-" + job.getId() + ".zip");
                ZipOutputStream zos = new ZipOutputStream(fos);) {

            // create byte buffer
            byte[] buffer = new byte[1024];
            String[] srcFiles = {"/vagrant/env/data/results/results-" + job.getId() + ".tsv", "/vagrant/env/data/results/results-plc-" + job.getId() + ".tsv"};

            for (String srcFile1 : srcFiles) {
                File srcFile = new File(srcFile1);
                // begin writing a new ZIP entry, positions the stream to the start of the entry data
                try (FileInputStream fis = new FileInputStream(srcFile)) {
                    // begin writing a new ZIP entry, positions the stream to the start of the entry data
                    zos.putNextEntry(new ZipEntry(srcFile.getName()));
                    int length;
                    while ((length = fis.read(buffer)) > 0) {
                        zos.write(buffer, 0, length);
                    }
                    zos.closeEntry();
                }
            }

        } catch (IOException ex) {
            logger.error("Error creating zip file for job: " + job.getId(), ex);
        }
    }

    private void emailResults(Job job) throws MessagingException {
        logger.info("Sending email for job:" + job.getId());
        Message message = new MimeMessage(mailSession);
        message.setFrom(new InternetAddress(CRESSEXPRESS_EMAIL));
        Address toAddress = new InternetAddress(job.getEmail());
        message.addRecipient(Message.RecipientType.TO, toAddress);
        message.setSubject("Cressexpress Results");
        message.setContent(getEmailMessageContent(job),
                "text/html");
        Transport.send(message);
    }

    private String getEmailMessageContent(Job job) {
        StringBuilder msg = new StringBuilder("<html>");
        msg.append("<link rel=\"stylesheet\" href=\"//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css\">");
        msg.append("<body>");
        msg.append("     <div>"
                + "        <h2>"
                + "          Thank you for using Cressexpress!"
                + "        </h2>"
                + "    </div>"
                + "      <hr/>"
                + "    <div class='well'>"
                + "      <h2>"
                + "        Your results are ready! "
                + "      </h2>"
                + "      <div><a class='btn btn-link' href='http://test.cressexpress.org/results/results-plc-" + job.getId() + ".tsv'>Download Pathway Level Coexpression Analysis Results</a></div>"
                + "      <div><a class='btn btn-link' href='http://test.cressexpress.org/results/results-" + job.getId() + ".tsv'>Download Raw Results</a></div>"
                + "    </div>"
        );
        msg.append("</body></html>");
        return msg.toString();
    }

    private List<Integer> getMicroarrayIndexes(List<String> microarrayOrder, List<String> microarraySelections) {
        List<Integer> indexList = new ArrayList<>();
        for (String selection : microarraySelections) {
            if (!microarrayOrder.contains(selection)) {
                logger.error("Microarray not in microarrayOrder collection:" + selection);
            } else {
                indexList.add(microarrayOrder.indexOf(selection));
            }
        }
        return indexList;
    }

    private List<String> retrieveMicroarrayOrder(Job job) {
        //TODO when needed replace with job's releaseVersion order 
        return releaseVersionReference.getDefaultMicroarrayOrder();
    }

    private void initializeExpressionData() {
        try (Reader in = new FileReader(EXPRESSION_DATA_FLAT_FILE)) {
            int i = 0;
            for (CSVRecord record : CSVFormat.DEFAULT.parse(in)) {
                for (int j = 0; j < record.size(); j++) {
                    expressionData[i][j] = Double.parseDouble(record.get(j));
                }
                i++;
            }
        } catch (IOException ex) {
            logger.error("IOException while loading expression data.", ex);
        }
    }

    public void appendExpressionData(Set<String> probesetNames, StringBuilder tsvContent) {
        List<Integer> probesetSelectionIndexes = probesetNames.parallelStream().map(probeset -> probesetIndexReference.get(probeset)).collect(toList());
        String TAB = "\t";
        String NEWLINE = "\n";
        for (int i = 0; i < defaultReleaseMicroarrays.size(); i++) {
            Microarray marray = defaultReleaseMicroarrays.get(i);
            tsvContent.append(marray.getName());
            tsvContent.append(TAB);
            tsvContent.append(marray.getSummary());
            tsvContent.append(TAB);
            for (Integer probesetIndex : probesetSelectionIndexes) {
                tsvContent.append(expressionData[probesetIndex][i]);
                tsvContent.append(TAB);
            }
            tsvContent.append(NEWLINE);
        }
    }
}
