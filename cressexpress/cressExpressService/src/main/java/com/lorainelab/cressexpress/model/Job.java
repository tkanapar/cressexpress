/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lorainelab.cressexpress.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author dcnorris
 */
@Entity
@Table(name = "job")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Job.findAll", query = "SELECT j FROM Job j"),
    @NamedQuery(name = "Job.findByEmail", query = "SELECT j FROM Job j WHERE j.email = :email"),
    @NamedQuery(name = "Job.findByPlcRsquared", query = "SELECT j FROM Job j WHERE j.plcRsquared = :plcRsquared"),
    @NamedQuery(name = "Job.findByDate", query = "SELECT j FROM Job j WHERE j.date = :date"),
    @NamedQuery(name = "Job.findById", query = "SELECT j FROM Job j WHERE j.id = :id")})
public class Job implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Email
    @Basic(optional = false)
    @Column(name = "email")
    private String email;
    
    //TODO convert to list of probesets since it is wasteful to map between this format and leftover from old implementation
    @NotBlank
    @Basic(optional = false)
    @Type(type="text")
    @Column(name = "probesetSelections")
    private String probesetSelections;
    
    //TODO convert to list of microarrays since it is wasteful to map between this format and leftover from old implementation
    @NotBlank
    @Basic(optional = false)
    @Type(type="text")
    @Column(name = "arraySelections")
    private String arraySelections;
    
    @Basic(optional = false)
    @Column(name = "plcRsquared")
    private float plcRsquared;
    @Basic(optional = false)
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "releaseVersion", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private ReleaseVersion releaseVersion;

    public Job() {
    }

    public Job(Integer id) {
        this.id = id;
    }

    public Job(Integer id, String email, String probesetSelections, String arraySelections, float plcRsquared, Date date) {
        this.id = id;
        this.email = email;
        this.probesetSelections = probesetSelections;
        this.arraySelections = arraySelections;
        this.plcRsquared = plcRsquared;
        this.date = date;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProbesetSelections() {
        return probesetSelections;
    }

    public void setProbesetSelections(String probesetSelections) {
        this.probesetSelections = probesetSelections;
    }

    public String getArraySelections() {
        return arraySelections;
    }

    public void setArraySelections(String arraySelections) {
        this.arraySelections = arraySelections;
    }

    public float getPlcRsquared() {
        return plcRsquared;
    }

    public void setPlcRsquared(float plcRsquared) {
        this.plcRsquared = plcRsquared;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ReleaseVersion getReleaseVersion() {
        return releaseVersion;
    }

    public void setReleaseVersion(ReleaseVersion releaseVersion) {
        this.releaseVersion = releaseVersion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Job)) {
            return false;
        }
        Job other = (Job) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lorainelab.cressdbbuilder.Job[ id=" + id + " ]";
    }
    
}
