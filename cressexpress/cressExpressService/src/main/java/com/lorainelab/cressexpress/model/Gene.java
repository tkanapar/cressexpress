/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab.cressexpress.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author dcnorris
 */
@Entity
@Table(name = "gene")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Gene.findAll", query = "SELECT g FROM Gene g"),
    @NamedQuery(name = "Gene.findById", query = "SELECT g FROM Gene g WHERE g.id = :id"),
    @NamedQuery(name = "Gene.findByName", query = "SELECT g FROM Gene g WHERE g.name = :name"),
    @NamedQuery(name = "Gene.findBySymbol", query = "SELECT g FROM Gene g WHERE g.symbol = :symbol"),
    @NamedQuery(name = "Gene.findByDescription", query = "SELECT g FROM Gene g WHERE g.description = :description")})
public class Gene implements Serializable {

    private static final long serialVersionUID = 1L;
   
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "symbol")
    private String symbol;
    
    @Column(name = "description")
    private String description;
    
    @JoinColumn(name = "species", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Species species;

    public Gene() {
    }

    public Gene(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Gene)) {
            return false;
        }
        Gene other = (Gene) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lorainelab.cressdbbuilder.Gene[ id=" + id + " ]";
    }

}
