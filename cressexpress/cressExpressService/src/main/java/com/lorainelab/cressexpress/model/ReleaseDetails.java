/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lorainelab.cressexpress.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dcnorris
 */
@Entity
@Table(name = "releaseDetails")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReleaseDetails.findAll", query = "SELECT r FROM ReleaseDetails r"),
    @NamedQuery(name = "ReleaseDetails.findByCelfile", query = "SELECT r FROM ReleaseDetails r WHERE r.celfile = :celfile"),
    @NamedQuery(name = "ReleaseDetails.findByReleaseVersion", query = "SELECT r FROM ReleaseDetails r WHERE r.releaseVersion = :releaseVersion"),
    @NamedQuery(name = "ReleaseDetails.findByMicroarray", query = "SELECT r FROM ReleaseDetails r WHERE r.microarray = :microarray"),
    @NamedQuery(name = "ReleaseDetails.findById", query = "SELECT r FROM ReleaseDetails r WHERE r.id = :id")})
public class ReleaseDetails implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "celfile")
    private String celfile;
    @Basic(optional = false)
    @Column(name = "releaseVersion")
    private int releaseVersion;
    @Basic(optional = false)
    @Column(name = "microarray")
    private int microarray;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    public ReleaseDetails() {
    }

    public ReleaseDetails(Integer id) {
        this.id = id;
    }

    public ReleaseDetails(Integer id, String celfile, int releaseVersion, int microarray) {
        this.id = id;
        this.celfile = celfile;
        this.releaseVersion = releaseVersion;
        this.microarray = microarray;
    }

    public String getCelfile() {
        return celfile;
    }

    public void setCelfile(String celfile) {
        this.celfile = celfile;
    }

    public int getReleaseVersion() {
        return releaseVersion;
    }

    public void setReleaseVersion(int releaseVersion) {
        this.releaseVersion = releaseVersion;
    }

    public int getMicroarray() {
        return microarray;
    }

    public void setMicroarray(int microarray) {
        this.microarray = microarray;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReleaseDetails)) {
            return false;
        }
        ReleaseDetails other = (ReleaseDetails) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lorainelab.cressdbbuilder.ReleaseDetails[ id=" + id + " ]";
    }
    
}
