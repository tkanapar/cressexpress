/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab.cressexpress.reference;

import com.lorainelab.cressexpress.controller.ReleaseVersionController;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import static javax.ejb.ConcurrencyManagementType.BEAN;
import javax.ejb.DependsOn;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dcnorris
 */
@Startup
@DependsOn("ProbesetInfoReference")
@ConcurrencyManagement(BEAN)
@Singleton
public class ResultInfoReference {

    private static final Logger logger = LoggerFactory.getLogger(ResultInfoReference.class);
    private String[][] resultInfoData;
    private List<String> probesetNames;
    
    @Inject
    private ProbesetInfoReference probesetInfoReference;
    
    @Inject
    private ReleaseVersionController releaseVersionController;

    @PostConstruct
    private void setup() {
        probesetNames = new ArrayList<>();
        probesetNames.addAll(probesetInfoReference.getProbesetNames());
        initializeResultInfo();
    }

    private void initializeResultInfo() {
        //use off heap map for initial map
        File resultInfoDataFile = new File("/vagrant/env/data/resultInfoData");
        DB releaseInfoDb = DBMaker.newFileDB(resultInfoDataFile).mmapFileEnable().transactionDisable().asyncWriteEnable().cacheSize(22810).cacheHardRefEnable().make();
        Map<String, Map<String, String>> resultInfoMap = releaseInfoDb.getTreeMap("resultInfoData");
        if (resultInfoMap.isEmpty()) {
            String query = "select distinct(p.name) as ps, p.uniq as 'Unique', p.Redundant, g.name as AGI, g.symbol as Symbol, g.description as Description from probeset p, gene g, probeset2geneMap p2m where p.id = p2m.probeset and g.id = p2m.gene";
            //get the hibernate session from the injected entity manager.
            Session session = (Session) releaseVersionController.getFacade().getEntityManager().getDelegate();
            final Query releaseInfoQuery = session.createSQLQuery(query);
            releaseInfoQuery.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
            List<Map<String, Object>> test = (List<Map<String, Object>>) releaseInfoQuery.list();
            for (Map<String, Object> map : test) {
                String probesetName = (String) map.get("ps");
                Map<String, String> geneInfo = new HashMap<>();
                geneInfo.put("ps", probesetName);
                geneInfo.put("Unique", (String) map.get("Unique"));
                geneInfo.put("Redundant", (String) map.get("Redundant"));
                geneInfo.put("AGI", (String) map.get("AGI"));
                geneInfo.put("Symbol", (String) map.get("Symbol"));
                geneInfo.put("Description", (String) map.get("Description"));
                resultInfoMap.put(probesetName, geneInfo);
            }
            //fill in any gaps with NA
            for (String probeset : probesetNames) {
                if (!resultInfoMap.containsKey(probeset)) {
                    Map<String, String> geneInfo = new HashMap<>();
                    geneInfo.put("ps", probeset);
                    geneInfo.put("Unique", "NA");
                    geneInfo.put("Redundant", "NA");
                    geneInfo.put("AGI", "NA");
                    geneInfo.put("Symbol", "NA");
                    geneInfo.put("Description", "NA");
                    resultInfoMap.put(probeset, geneInfo);
                }
            }
            releaseInfoDb.compact();
            releaseInfoDb.commit();
        }
        //copy to 2d array
        resultInfoData = new String[22_810][6];
        String[] infoAttributes = {"ps", "Unique", "Redundant", "AGI", "Symbol", "Description"};
        for (int i = 0; i < probesetNames.size(); i++) {
            for (int j = 0; j < infoAttributes.length; j++) {
                resultInfoData[i][j] = resultInfoMap.get(probesetNames.get(i)).get(infoAttributes[j]);
            }
        }
    }

    public String[][] getResultInfoData() {
        return resultInfoData;
    }

}
