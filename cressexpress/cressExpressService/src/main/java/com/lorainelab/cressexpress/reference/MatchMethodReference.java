/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab.cressexpress.reference;

import com.lorainelab.cressexpress.controller.MatchMethodController;
import com.lorainelab.cressexpress.model.MatchMethod;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import static javax.ejb.ConcurrencyManagementType.BEAN;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

/**
 *
 * @author dcnorris
 */
@Startup
@ConcurrencyManagement(BEAN)
@Singleton
public class MatchMethodReference {

    @Inject
    private MatchMethodController matchMethodController;

    private List<MatchMethod> matchMethodOptions;

    @PostConstruct
    public void setup() {
        initializeMatchMethodOptions();
    }

    //TODO for now we must limit results since we don't have mapping for all matchMethods in DB
    private void initializeMatchMethodOptions() {
        matchMethodOptions = matchMethodController.findMatchMethodEntities(1, 1);
    }

    public List<MatchMethod> getMatchMethodOptions() {
        return matchMethodOptions;
    }

}
