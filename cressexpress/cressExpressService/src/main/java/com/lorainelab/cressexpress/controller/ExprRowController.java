package com.lorainelab.cressexpress.controller;

import com.lorainelab.cressexpress.controller.exceptions.NonexistentEntityException;
import com.lorainelab.cressexpress.facade.ExprRowFacade;
import com.lorainelab.cressexpress.model.ExprRow;
import com.lorainelab.cressexpress.model.Probeset;
import com.lorainelab.cressexpress.model.ReleaseVersion;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Stateless
@LocalBean
public class ExprRowController implements Serializable {

    @EJB
    private ExprRowFacade ejbFacade;

    public ExprRowController() {
    }

    private EntityManager getEntityManager() {
        return ejbFacade.getEntityManager();
    }

    public void create(ExprRow exprRow) {
        EntityManager em = null;
        em = getEntityManager();
        em.getTransaction().begin();
        ReleaseVersion releaseVersion = exprRow.getReleaseVersion();
        if (releaseVersion != null) {
            releaseVersion = em.getReference(releaseVersion.getClass(), releaseVersion.getId());
            exprRow.setReleaseVersion(releaseVersion);
        }
        Probeset probeset = exprRow.getProbeset();
        if (probeset != null) {
            probeset = em.getReference(probeset.getClass(), probeset.getId());
            exprRow.setProbeset(probeset);
        }
        em.persist(exprRow);
        if (releaseVersion != null) {
            releaseVersion.getExprRowSet().add(exprRow);
            releaseVersion = em.merge(releaseVersion);
        }
        if (probeset != null) {
            probeset.getExprRowSet().add(exprRow);
            probeset = em.merge(probeset);
        }
        em.getTransaction().commit();

    }

    public void edit(ExprRow exprRow) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            ExprRow persistentExprRow = em.find(ExprRow.class, exprRow.getId());
            ReleaseVersion releaseVersionOld = persistentExprRow.getReleaseVersion();
            ReleaseVersion releaseVersionNew = exprRow.getReleaseVersion();
            Probeset probesetOld = persistentExprRow.getProbeset();
            Probeset probesetNew = exprRow.getProbeset();
            if (releaseVersionNew != null) {
                releaseVersionNew = em.getReference(releaseVersionNew.getClass(), releaseVersionNew.getId());
                exprRow.setReleaseVersion(releaseVersionNew);
            }
            if (probesetNew != null) {
                probesetNew = em.getReference(probesetNew.getClass(), probesetNew.getId());
                exprRow.setProbeset(probesetNew);
            }
            exprRow = em.merge(exprRow);
            if (releaseVersionOld != null && !releaseVersionOld.equals(releaseVersionNew)) {
                releaseVersionOld.getExprRowSet().remove(exprRow);
                releaseVersionOld = em.merge(releaseVersionOld);
            }
            if (releaseVersionNew != null && !releaseVersionNew.equals(releaseVersionOld)) {
                releaseVersionNew.getExprRowSet().add(exprRow);
                releaseVersionNew = em.merge(releaseVersionNew);
            }
            if (probesetOld != null && !probesetOld.equals(probesetNew)) {
                probesetOld.getExprRowSet().remove(exprRow);
                probesetOld = em.merge(probesetOld);
            }
            if (probesetNew != null && !probesetNew.equals(probesetOld)) {
                probesetNew.getExprRowSet().add(exprRow);
                probesetNew = em.merge(probesetNew);
            }

        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = exprRow.getId();
                if (findExprRow(id) == null) {
                    throw new NonexistentEntityException("The exprRow with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        em = getEntityManager();
        ExprRow exprRow;
        try {
            exprRow = em.getReference(ExprRow.class, id);
            exprRow.getId();
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The exprRow with id " + id + " no longer exists.", enfe);
        }
        ReleaseVersion releaseVersion = exprRow.getReleaseVersion();
        if (releaseVersion != null) {
            releaseVersion.getExprRowSet().remove(exprRow);
            releaseVersion = em.merge(releaseVersion);
        }
        Probeset probeset = exprRow.getProbeset();
        if (probeset != null) {
            probeset.getExprRowSet().remove(exprRow);
            probeset = em.merge(probeset);
        }
        em.remove(exprRow);
    }

    public List<ExprRow> findExprRowEntities() {
        return findExprRowEntities(true, -1, -1);
    }

    public List<ExprRow> findExprRowEntities(int maxResults, int firstResult) {
        return findExprRowEntities(false, maxResults, firstResult);
    }

    private List<ExprRow> findExprRowEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();

        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(ExprRow.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public ExprRow findExprRow(Integer id) {
        EntityManager em = getEntityManager();
        return em.find(ExprRow.class, id);
    }

    public int getExprRowCount() {
        EntityManager em = getEntityManager();
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<ExprRow> rt = cq.from(ExprRow.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
