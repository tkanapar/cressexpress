package com.lorainelab.cressexpress.controller;

import com.lorainelab.cressexpress.facade.MatchMethodFacade;
import com.lorainelab.cressexpress.model.MatchMethod;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Stateless
@LocalBean
public class MatchMethodController implements Serializable {

    @EJB
    private MatchMethodFacade ejbFacade;

    public MatchMethodController() {
    }

    public MatchMethod findById(Integer id) {
        return findMatchMethod(id);
    }

    public List<MatchMethod> findMatchMethodEntities() {
        return findMatchMethodEntities(true, -1, -1);
    }

    public List<MatchMethod> findMatchMethodEntities(int maxResults, int firstResult) {
        return findMatchMethodEntities(false, maxResults, firstResult);
    }

    private List<MatchMethod> findMatchMethodEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = ejbFacade.getEntityManager();
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(MatchMethod.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();

    }

    public MatchMethod findMatchMethod(Integer id) {
        EntityManager em = ejbFacade.getEntityManager();
        return em.find(MatchMethod.class, id);
    }

    public int getMatchMethodCount() {
        EntityManager em = ejbFacade.getEntityManager();
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<MatchMethod> rt = cq.from(MatchMethod.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
