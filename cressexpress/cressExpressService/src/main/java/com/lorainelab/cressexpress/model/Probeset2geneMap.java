/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lorainelab.cressexpress.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dcnorris
 */
@Entity
@Table(name = "probeset2geneMap")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Probeset2geneMap.findAll", query = "SELECT p FROM Probeset2geneMap p"),
    @NamedQuery(name = "Probeset2geneMap.findById", query = "SELECT p FROM Probeset2geneMap p WHERE p.id = :id")})
public class Probeset2geneMap implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "probeset", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Probeset probeset;
    @JoinColumn(name = "matchMethod", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private MatchMethod matchMethod;
    @JoinColumn(name = "gene", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Gene gene;

    public Probeset2geneMap() {
    }

    public Probeset2geneMap(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Probeset getProbeset() {
        return probeset;
    }

    public void setProbeset(Probeset probeset) {
        this.probeset = probeset;
    }

    public MatchMethod getMatchMethod() {
        return matchMethod;
    }

    public void setMatchMethod(MatchMethod matchMethod) {
        this.matchMethod = matchMethod;
    }

    public Gene getGene() {
        return gene;
    }

    public void setGene(Gene gene) {
        this.gene = gene;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Probeset2geneMap)) {
            return false;
        }
        Probeset2geneMap other = (Probeset2geneMap) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lorainelab.cressdbbuilder.Probeset2geneMap[ id=" + id + " ]";
    }
    
}
