package com.lorainelab.cressexpress.controller;

import com.lorainelab.cressexpress.facade.MicroarrayFacade;
import com.lorainelab.cressexpress.model.Microarray;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;

@Stateless
@LocalBean
public class MicroarrayController implements Serializable {

    @EJB
    private MicroarrayFacade ejbFacade;

    public MicroarrayController() {
    }

    private MicroarrayFacade getFacade() {
        return ejbFacade;
    }

    public List<MicroarrayController> findMicroarrayEntities() {
        return findMicroarrayEntities(true, -1, -1);
    }

    public List<MicroarrayController> findMicroarrayEntities(int maxResults, int firstResult) {
        return findMicroarrayEntities(false, maxResults, firstResult);
    }

    private List<MicroarrayController> findMicroarrayEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getFacade().getEntityManager();

        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(MicroarrayController.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();

    }

    public MicroarrayController findMicroarray(Integer id) {
        EntityManager em = getFacade().getEntityManager();
        return em.find(MicroarrayController.class, id);
    }

    public Microarray findByName(String name) {
        EntityManager em = getFacade().getEntityManager();
        TypedQuery<Microarray> query = em.createNamedQuery("Microarray.findByName", Microarray.class);
        query.setParameter("name", name);
        return query.getSingleResult();
    }
}
