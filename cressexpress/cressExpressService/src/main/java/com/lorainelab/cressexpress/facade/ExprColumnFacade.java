/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lorainelab.cressexpress.facade;

import com.lorainelab.cressexpress.model.ExprColumn;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author dcnorris
 */
@Stateless
public class ExprColumnFacade extends AbstractFacade<ExprColumn> {
    @PersistenceContext(unitName = "CressExpress")
    private EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    public ExprColumnFacade() {
        super(ExprColumn.class);
    }
    
}
