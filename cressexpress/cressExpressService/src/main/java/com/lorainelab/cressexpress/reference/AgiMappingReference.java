/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab.cressexpress.reference;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.SetMultimap;
import com.lorainelab.cressexpress.controller.GeneController;
import com.lorainelab.cressexpress.controller.Probeset2geneMapController;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import static javax.ejb.ConcurrencyManagementType.BEAN;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

/**
 *
 * @author dcnorris
 */
@Startup
@ConcurrencyManagement(BEAN)
@Singleton
public class AgiMappingReference {

    @Inject
    private Probeset2geneMapController probeset2geneController;

    @Inject
    private GeneController geneController;

    private List<String> validAGINames;
    private SetMultimap<String, String> agiToProbesetMap;

    @PostConstruct
    public void setup() {
        agiToProbesetMap = probeset2geneController.getAgiToProbesetMap();
        validAGINames = ImmutableList.<String>builder().addAll(geneController.getValidGeneNames()).build();
    }
    
        //will need to change to table or map of maps when multiple match methods are supported
    public SetMultimap<String, String> getAgiToProbesetMapping() {
        return agiToProbesetMap;
    }

    public List<String> getValidAGINames() {
        return validAGINames;
    }
}
