/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab.cressexpress.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author dcnorris
 */
@Entity
@Table(name = "probeset")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Probeset.findAll", query = "SELECT p FROM Probeset p"),
    @NamedQuery(name = "Probeset.findById", query = "SELECT p FROM Probeset p WHERE p.id = :id"),
    @NamedQuery(name = "Probeset.findByName", query = "SELECT p FROM Probeset p WHERE p.name = :name")})
public class Probeset implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @JoinColumn(name = "platform_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Platform platformId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "probeset")
    private Set<Probeset2geneMap> probeset2geneMapSet;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "probeset")
    private Set<ExprRow> exprRowSet;
    @Column(name = "uniq")
    private String uniq;
    @Column(name = "redundant")
    private String redundant;

    public Probeset() {
    }

    public Probeset(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Platform getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Platform platformId) {
        this.platformId = platformId;
    }

    public String getUniq() {
        return uniq;
    }

    public void setUniq(String uniq) {
        this.uniq = uniq;
    }

    public String getRedundant() {
        return redundant;
    }

    public void setRedundant(String redundant) {
        this.redundant = redundant;
    }

    @XmlTransient
    public Set<Probeset2geneMap> getProbeset2geneMapSet() {
        return probeset2geneMapSet;
    }

    public void setProbeset2geneMapSet(Set<Probeset2geneMap> probeset2geneMapSet) {
        this.probeset2geneMapSet = probeset2geneMapSet;
    }

    @XmlTransient
    public Set<ExprRow> getExprRowSet() {
        return exprRowSet;
    }

    public void setExprRowSet(Set<ExprRow> exprRowSet) {
        this.exprRowSet = exprRowSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Probeset)) {
            return false;
        }
        Probeset other = (Probeset) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lorainelab.cressdbbuilder.Probeset[ id=" + id + " ]";
    }

}
