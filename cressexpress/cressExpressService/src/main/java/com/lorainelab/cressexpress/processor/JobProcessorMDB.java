/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab.cressexpress.processor;

import com.lorainelab.cressexpress.model.Job;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dcnorris
 */
@MessageDriven(name = "JobProcessorMDB", activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "queue/JobProccessor"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
    @ActivationConfigProperty(propertyName = "transactionTimeout", propertyValue = "500"),
    @ActivationConfigProperty(propertyName = "maxSession", propertyValue = "1"),
    @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge")})
public class JobProcessorMDB implements MessageListener {

    private static final Logger logger = LoggerFactory.getLogger(JobProcessorMDB.class);

    @Inject
    private DefaultJobProcessor defaultJobProcessor;

    private static final String DEFAULT_RELEASE_VERSION = "4.0";

    @Override
    public void onMessage(Message msg) {
        logger.info("Received Job Message");
        try {
            Job job = msg.getBody(Job.class);
            if (job.getReleaseVersion().getName().equals(DEFAULT_RELEASE_VERSION)) {
                defaultJobProcessor.process(job);
            } else {
                logger.error("No job processor for release version: " + job.getReleaseVersion().getName());
            }
        } catch (JMSException ex) {
            logger.error("Error extracting Job Entity from JMS MEssage", ex);
        }
    }

}
