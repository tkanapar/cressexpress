package com.lorainelab.cressexpress.controller;

import com.google.common.primitives.Ints;
import com.lorainelab.cressexpress.facade.ExprColumnFacade;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;

@Stateless
@LocalBean
public class ExprColumnController implements Serializable {

    @EJB
    private ExprColumnFacade ejbFacade;

    public ExprColumnController() {
    }

    public Integer getCountByReleaseVersionId(Integer i) {
        EntityManager entityManager = ejbFacade.getEntityManager();
        Query countQuery = entityManager.createQuery("select count(*) from ExprColumn e WHERE e.releaseVersion.id =:versionId");
        countQuery.setParameter("versionId", i);
        Integer count = Ints.checkedCast((Long) countQuery.getSingleResult());
        return count;
    }

}
