/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template importedFile, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab.cressexpress.view;

import com.google.common.base.Joiner;
import com.lorainelab.cressexpress.model.Experiment;
import com.lorainelab.cressexpress.model.Microarray;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dcnorris
 */
@Named
@Stateful
@SessionScoped
public class MicroarraySelections implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = LoggerFactory.getLogger(MicroarraySelections.class);

    private Map<Experiment, Set<Microarray>> selectedArrays;
    //Note this exist to filter out Microarrays already selected but mapped to another experiment
    private Set<Microarray> duplicateTracker;

    private int selectionCount;

    @PostConstruct
    private void init() {
        selectedArrays = new HashMap<>();
        duplicateTracker = new HashSet<>();
        selectionCount = 0;
    }

    public void clearSelectedArrays() {
        selectedArrays.clear();
        duplicateTracker.clear();
        selectionCount = 0;
    }

    public void addMircoarray(Experiment experiment, Microarray microarray) {
        if (!duplicateTracker.contains(microarray)) {
            selectionCount++;
            Set<Microarray> toAdd = new HashSet<>();
            if (selectedArrays.containsKey(experiment)) {
                toAdd = selectedArrays.get(experiment);
            }

            toAdd.add(microarray);
            duplicateTracker.add(microarray);
            selectedArrays.put(experiment, toAdd);
        }
    }

    public void removeMircoarray(Experiment experiment, Microarray microarray) {
        if (selectedArrays.containsKey(experiment)) {
            Set<Microarray> currentList = selectedArrays.get(experiment);
            currentList.remove(microarray);
            duplicateTracker.remove(microarray);
            selectedArrays.put(experiment, currentList);
            selectionCount--;
        }
    }

    public boolean contains(Experiment experiment, Microarray microarray) {
        if (selectedArrays.containsKey(experiment)) {
            return selectedArrays.get(experiment).contains(microarray);
        } else {
            return false;
        }
    }

    public Integer getSelectionCount() {
        return selectionCount;
    }

    private List<String> getSelectedArrayMetaData() {
        List<String> selectionMetaData = new ArrayList<>();
        //add header line
        selectionMetaData.add("GSM ID" + "\t" + "GSE ID" + "\t" + "GSM DESCRIPTION" + "\t" + "GSE DESCRIPTION");
        for (Map.Entry<Experiment, Set<Microarray>> entry : selectedArrays.entrySet()) {
            String gseID = entry.getKey().getName();
            String gseDescription = entry.getKey().getSummary();
            for (Microarray marray : entry.getValue()) {
                String gsmID = marray.getName();
                String gsmDescription = marray.getSummary();
                selectionMetaData.add(gsmID + "\t" + gseID + "\t" + gsmDescription + "\t" + gseDescription + "\t");
            }
        }
        return selectionMetaData;
    }

    public List<String> getSelectedArrayIdentifiers() {
        List<String> arrayIdentifiers = new ArrayList<>();
        for (Set<Microarray> experimentMicroarraysList : selectedArrays.values()) {
            for (Microarray microarray : experimentMicroarraysList) {
                arrayIdentifiers.add(microarray.getName());
            }
        }
        return arrayIdentifiers;
    }

    public StreamedContent getExportSelectionsToFile() {
        List<String> metaData = getSelectedArrayMetaData();
        InputStream is = new ByteArrayInputStream(Joiner.on("\n").join(metaData).getBytes());
        StreamedContent file = new DefaultStreamedContent(is, "text/csv", "cressExpressMicroarraySelections.csv");
        return file;
    }

}
