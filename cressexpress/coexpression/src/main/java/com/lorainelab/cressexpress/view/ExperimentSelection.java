/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lorainelab.cressexpress.view;

import com.lorainelab.cressexpress.controller.ExperimentController;
import com.lorainelab.cressexpress.model.Experiment;
import com.lorainelab.cressexpress.model.Microarray;
import com.lorainelab.cressexpress.reference.ExperimentDetailsReference;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dcnorris
 */
@Named
@Stateful
@SessionScoped
public class ExperimentSelection implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = LoggerFactory.getLogger(ExperimentSelection.class);

    @Inject
    private ExperimentController controller;

    @Inject
    private ExperimentDetailsReference experimentDetailsReference;

    @Inject
    private MicroarraySelections microarraySelections;

    private List<Experiment> matchingExperiments;
    private Map<Experiment, Boolean> experimentSelectionMap;
    private Map<Microarray, Boolean> arraySelectionMap;
    private UploadedFile importFile;
    private Map<Experiment, Set<Microarray>> defaultReleaseMicroarrayData;
    private String query;
    private boolean selectAllMatchingExperiments = false;
    private Microarray selectedMicroarray;

    @PostConstruct
    private void init() {
        defaultReleaseMicroarrayData = experimentDetailsReference.getDefaultReleaseMicroarrayData();
        matchingExperiments = new ArrayList();
        //TODO this will obviously need to be dynamic when additional releases are added
        matchingExperiments.addAll(defaultReleaseMicroarrayData.keySet());
        //initSelectionMaps
        globalSelectionUpdate(Boolean.FALSE);
    }

    public UploadedFile getImportFile() {
        return importFile;
    }

    public void setImportFile(UploadedFile importFile) {
        this.importFile = importFile;
    }

    public void processImportFile() throws IOException {
        if (importFile == null) {
            return;
        }
        List<String> importedMicroarayNames = new ArrayList<>();
        try (InputStreamReader in = new InputStreamReader(importFile.getInputstream(), "UTF-8")) {
            int i = 0;
            for (CSVRecord record : CSVFormat.TDF.parse(in)) {
                //skip header
                if (i > 0) {
                    importedMicroarayNames.add(record.get(0));
                }
                i++;
            }
        } catch (IOException ex) {
            logger.error("Error processing microarray import file.", ex);
        }

        experimentSelectionMap = new HashMap<>();
        arraySelectionMap = new HashMap<>();
        matchingExperiments = new ArrayList();
        microarraySelections.clearSelectedArrays();
        for (Map.Entry<Experiment, Set<Microarray>> entry : defaultReleaseMicroarrayData.entrySet()) {
            for (Microarray validMicroarray : entry.getValue()) {
                for (String microarrayName : importedMicroarayNames) {
                    if (StringUtils.equalsIgnoreCase(microarrayName, validMicroarray.getName())) {
                        microarraySelections.addMircoarray(entry.getKey(), validMicroarray);
                        if (!matchingExperiments.contains(entry.getKey())) {
                            matchingExperiments.add(entry.getKey());
                        }
                    }
                }
            }
        }

        //Update exerimentSelectionMap
        for (Experiment experiment : matchingExperiments) {
            boolean allSelected = true;
            for (Microarray microarray : defaultReleaseMicroarrayData.get(experiment)) {
                if (microarraySelections.contains(experiment, microarray)) {
                    arraySelectionMap.put(microarray, true);
                } else {
                    arraySelectionMap.put(microarray, false);
                    allSelected = false;
                }
            }
            experimentSelectionMap.put(experiment, allSelected);
        }

    }

    public String getQuery() {
        if (StringUtils.isBlank(query)) {
            query = "";
        }
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
        updateExperiments();
    }

    public List<Experiment> getMatchingExperiments() {
        return matchingExperiments;
    }

    private void updateExperiments() {
        if (StringUtils.isNotBlank(query)) {
            List<Experiment> searchResultList = controller.luceneSearch(query);
            matchingExperiments = new ArrayList<>();
            //filter microarrays not in release
            for (Experiment experiment : searchResultList) {
                if (defaultReleaseMicroarrayData.containsKey(experiment)) {
                    matchingExperiments.add(experiment);
                }
            }
        }
    }

    private void globalSelectionUpdate(Boolean bool) {
        experimentSelectionMap = new HashMap<>();
        arraySelectionMap = new HashMap<>();
        microarraySelections.clearSelectedArrays();
        for (Experiment experiment : defaultReleaseMicroarrayData.keySet()) {
            experimentSelectionMap.put(experiment, bool);
            for (Microarray microarray : defaultReleaseMicroarrayData.get(experiment)) {
                arraySelectionMap.put(microarray, bool);
                if (bool) {
                    microarraySelections.addMircoarray(experiment, microarray);
                }
            }
        }
    }

    public void toggleAddAllMicroarrays(Experiment experiment) {
        boolean current = experimentSelectionMap.get(experiment);
        experimentSelectionMap.remove(experiment);
        experimentSelectionMap.put(experiment, current);
        for (Microarray microarray : defaultReleaseMicroarrayData.get(experiment)) {
            arraySelectionMap.remove(microarray);
            arraySelectionMap.put(microarray, current);
            if (current) {
                microarraySelections.addMircoarray(experiment, microarray);
            } else {
                microarraySelections.removeMircoarray(experiment, microarray);
            }
        }
    }
    
    public void toggleMicroArraySelection(Experiment experiment) {
        boolean current = !arraySelectionMap.get(selectedMicroarray);
        arraySelectionMap.remove(selectedMicroarray);
        arraySelectionMap.put(selectedMicroarray, current);
        if (current) {
            microarraySelections.addMircoarray(experiment, selectedMicroarray);
        } else {
            microarraySelections.removeMircoarray(experiment, selectedMicroarray);
        }
    }

    public void toggleMicroArraySelection(Experiment experiment, Microarray microarray) {
        boolean current = arraySelectionMap.get(microarray);
        arraySelectionMap.remove(microarray);
        arraySelectionMap.put(microarray, current);
        if (current) {
            microarraySelections.addMircoarray(experiment, microarray);
        } else {
            microarraySelections.removeMircoarray(experiment, microarray);
        }
    }

    public void selectAllMatching() {
        for (Experiment experiment : matchingExperiments) {
            experimentSelectionMap.remove(experiment);
            experimentSelectionMap.put(experiment, true);
            for (Microarray microarray : defaultReleaseMicroarrayData.get(experiment)) {
                arraySelectionMap.remove(microarray);
                arraySelectionMap.put(microarray, true);
                microarraySelections.addMircoarray(experiment, microarray);

            }
        }
    }

    public void selectAllExperiments() {
        query = "";
        matchingExperiments = new ArrayList();
        matchingExperiments.addAll(defaultReleaseMicroarrayData.keySet());
    }

    public void selectAllArraysGlobal() {
        globalSelectionUpdate(Boolean.TRUE);
    }

    public void clearAllSelections() {
        globalSelectionUpdate(Boolean.FALSE);
    }

    public Map<Experiment, Boolean> getExperimentSelectionMap() {
        return experimentSelectionMap;
    }

    public Map<Microarray, Boolean> getArraySelectionMap() {
        return arraySelectionMap;
    }

    public Microarray getSelectedMicroarray() {
        return selectedMicroarray;
    }

    public void setSelectedMicroarray(Microarray selectedMicroarray) {
        this.selectedMicroarray = selectedMicroarray;
    }

    public void navigateNext() throws IOException {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        context.redirect(context.getRequestContextPath() + "/step4.xhtml");
    }

    public void navigatePrev() throws IOException {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        context.redirect(context.getRequestContextPath() + "/step2.xhtml");
    }

    public boolean isSelectAllMatchingExperiments() {
        return selectAllMatchingExperiments;
    }

    public void setSelectAllMatchingExperiments(boolean selectAllMatchingExperiments) {
        this.selectAllMatchingExperiments = selectAllMatchingExperiments;
    }

}
