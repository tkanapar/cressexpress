#!/usr/bin/env python 

##################################
# Object of this script is to fetch the names of all the GSMs which have a CEL file associated with them for the platform GPL198.
# Steps followed for getting the names of GSMs are given below:

# When looking for data relating to a specific array, it is usually safest to use that Platform's GEO accession number, rather than its name. 
# Construct and perform an eSearch query in db=gds for all Series records that have Samples relating to GPL198 and have CEL files, using:
# http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=gds&term=GPL198[ACCN]+AND+gse[ETYP]+AND+cel[suppFile]&retmax=5000&usehistory=y
# Use the query_key and WebEnv parameters from the eSearch to perform an eSummary:

# http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=gds&query_key=X&WebEnv=ENTER_WEBENV_PARAMETER_HERE


#Extract the Series accession numbers from the eSummary document. You can then use this Series accession list to construct URLs to get the raw data files, for example:

#ftp://ftp.ncbi.nlm.nih.gov/geo/series/GSE5nnn/GSE5290/suppl/GSE5290_RAW.tar

# The above information has been obtained from http://www.ncbi.nlm.nih.gov/geo/info/geo_paccess.html#ExampleIII 

#
#
#
#
#
import sys,urllib
import requests
from lxml import etree as ET
import re

def main(url=None):
    getData(url=url)
   # print txt

def getData(url=None):
    try:
         headers={'accept':'application/xml'}
         print 'Headers Defined'
       
         response = requests.get(url, headers=headers, stream=True)
         print url
 #       response.raise_for_status()
         print 'Response Received'
         doc = ET.parse(response.raw)
         itemlist = doc.xpath("/eSummaryResult/DocSum/Item[@Name='Samples']/Item[@Name='Sample']/Item[@Name='Accession']")
         print len(itemlist)
         for s in itemlist:
          print s.text
         
    except Exception as e:
        print e
        e2 = Exception('Error accessing document at %s' % (url,))
        e2.cause = e
        raise e2

    #filestream=urllib.urlopen(url)
    #txt=filestream.read()
    #return txt

if __name__ == '__main__':
     url=sys.argv[1]
 #   nameofprogram=sys.argv[0]
  #  sys.stderr.write("%s I got an argument: %s\n"%(nameofprogram,url))
     main(url)
