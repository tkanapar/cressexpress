#!/usr/bin/env python 

##################################
#This script maps all the GSM ids to their CEL files.The script first fetches names of all the CEL files from server , finds their associated CEL files in the local directory 
#and then writes the mapping to a text file
#This file takes 3 parameters of which the last one is optional. They are as follows:

#1)Platform ID: The platform for which the CEL files need to be mapped:
#2)Path To CEL files: Full path to existing CEL files for the given platform. eg "/Users/lorainelab/RawDataFiles/suppl/"
#3)Outputfile Name(Optional): Path to the output file to which the mapping will be written.

#The URL that we are fetching the CEL file info is :
#"http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=gds&term=GPL198[ACCN]+AND+gsm[ETYP]+AND+cel[suppFile]&retmax=100000&usehistory=y"
#We are fetching CEL files for the platform GPL198 in the above URL

import sys,urllib
import requests
from lxml import etree as ET
import re
import subprocess
import os
import glob

OutputFile="GSM_ID_to_CEL_files_Mapping.txt"
PathToExistingCELfiles=""
def main(URL=None):
    getData(URL=URL)
  

def getData(URL=None):
    
        headers={'accept':'application/xml'}
        response = requests.get(URL, headers=headers, stream=True)
        print 'Response Received'
        doc = ET.parse(response.raw)
        itemlist = doc.xpath("/eSearchResult/IdList/Id")# Extracting all the GSM IDs from the XML
        print "Total number of GSM IDs: "+str(len(itemlist))
        print "Please Wait..."
        f = open(OutputFile,'w')
        
        id=""
        for s in itemlist:
            k = str(s.text)   
            for ch in range(1,len(k)): #This loop gets the GSM id after stripping off unneeded digits
                if k[ch]!='0':
                    id=k[ch:]
                    break
            k = str(id)
            k = k[:len(k)-3]
            k=k+"nnn"
            
            #Check if GSM is present in our directory
                    
            try:
                
                link="/geo/samples/GSM%s/GSM%s/suppl/"
                link = link%(str(k),id)
                arr= glob.glob(PathToExistingCELfiles+"[Gg][Ss][Mm]"+id+"*.[Cc][Ee][Ll].gz") #The GSM cel files have inconsistent names.
                GSM_ID="GSM"+id
                GSM_ID=str(GSM_ID)
                GSM_ID=GSM_ID.strip()
                
                for cel_file in arr:
                    #print ()
                    filename = os.path.basename(cel_file)
                    filename = filename.strip()
                    lengthOfGSM_ID=len(GSM_ID) #Here we check whether the CEL file we are associating with a GSM ID does indeed belong to that GSM ID. eg: GSM1234*.CEL.gz will return GSM1234.CEL.gz & GSM12345.CEL.gz and so on. So we take the first choice only
                    character = filename[lengthOfGSM_ID]
                    if character.isdigit(): #CEL file does not belong to the GSM ID in context
                        continue
                    f.write(GSM_ID)
                    f.write("\t"+filename)
                    f.write("\n")    
                #if len(arr)==0:
                    #it means file does not exist in our directory . So we write it to the output file.
                 #        f.write(link+"\n")
                #if len(arr)>1:
                   # print link
                             
                          
            except Exception as e:
                print e
                print "Something went wrong in the script!!"  
                                         
        print "The mapping has been written to : "+OutputFile
        
         
    
if __name__ == '__main__':
      global OutputFile  
      global PathToExistingCELfiles
      platformID=sys.argv[1]
      PathToExistingCELfiles=sys.argv[2]
      if len(sys.argv)==4:
          OutputFile=str(sys.argv[3])
      url = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=gds&term="+platformID+"[ACCN]+AND+gsm[ETYP]+AND+cel[suppFile]&retmax=100000&usehistory=y"
      main(url)
