CREATE DATABASE  IF NOT EXISTS `coexpression2` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `coexpression2`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: localhost    Database: coexpression2
-- ------------------------------------------------------
-- Server version	5.0.95-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Not dumping tablespaces as no INFORMATION_SCHEMA.FILES table on this server
--

--
-- Table structure for table `array`
--

DROP TABLE IF EXISTS `array`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `array` (
  `id` int(11) NOT NULL,
  `name` varchar(64) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `array_attribute`
--

DROP TABLE IF EXISTS `array_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `array_attribute` (
  `array_id` int(11) default NULL,
  `namespace` varchar(64) default NULL,
  `name` varchar(64) default NULL,
  `value` varchar(512) default NULL,
  KEY `fk_arr_id` (`array_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `assembly`
--

DROP TABLE IF EXISTS `assembly`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assembly` (
  `id` int(11) NOT NULL,
  `species_id` int(11) default NULL,
  `date` date default NULL,
  `name` varchar(64) default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_species_id` (`species_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `experiment`
--

DROP TABLE IF EXISTS `experiment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `experiment` (
  `id` int(11) NOT NULL,
  `name` varchar(64) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `experiment2array`
--

DROP TABLE IF EXISTS `experiment2array`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `experiment2array` (
  `experiment_id` int(11) default NULL,
  `array_id` int(11) default NULL,
  KEY `fk_array_id` (`experiment_id`),
  KEY `fk_exp_id` (`array_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `experiment_attribute`
--

DROP TABLE IF EXISTS `experiment_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `experiment_attribute` (
  `experiment_id` int(11) NOT NULL,
  `name` varchar(64) default NULL,
  `value` varchar(7000) default NULL,
  `namespace` varchar(64) default NULL,
  KEY `fk_exp_id` (`experiment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `expr_col`
--

DROP TABLE IF EXISTS `expr_col`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expr_col` (
  `release_id` int(11) default NULL,
  `array_id` int(11) default NULL,
  `array_name` varchar(64) default NULL,
  `data` mediumblob,
  KEY `fk_release_id` (`release_id`),
  KEY `fk_array_id` (`array_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `expr_row`
--

DROP TABLE IF EXISTS `expr_row`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expr_row` (
  `release_id` int(11) default NULL,
  `probeset_id` int(11) default NULL,
  `probeset_name` varchar(64) default NULL,
  `data` mediumblob,
  KEY `fk_release_id` (`release_id`),
  KEY `fk_probeset_id` (`probeset_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gene`
--

DROP TABLE IF EXISTS `gene`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gene` (
  `id` int(11) NOT NULL,
  `assembly_id` int(11) default NULL,
  `species_id` int(11) default NULL,
  `name` varchar(32) default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_species_id` (`species_id`),
  KEY `fk_assembly_id` (`assembly_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gene_info`
--

DROP TABLE IF EXISTS `gene_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gene_info` (
  `id` int(11) default NULL,
  `symbol` varchar(32) default NULL,
  `descr` varchar(1024) default NULL,
  KEY `fk_id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gsm2gsm_file`
--

DROP TABLE IF EXISTS `gsm2gsm_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gsm2gsm_file` (
  `release_id` int(11) NOT NULL,
  `array_id` int(11) default NULL,
  `name` varchar(30) default NULL,
  `celfile` varchar(60) default NULL,
  KEY `fk_release_id` (`release_id`),
  KEY `fk_array_id` (`array_id`)
) ENGINE=MyISAM DEFAULT CHARSET=euckr;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `job`
--

DROP TABLE IF EXISTS `job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job` (
  `email` varchar(200) NOT NULL,
  `release_id` int(10) default NULL,
  `bait_strings` varchar(1000) default NULL,
  `array_selections` blob,
  `plc_rsquared` float default NULL,
  `date` date default NULL,
  `tracking_id` varchar(25) default NULL,
  `is_complete` tinyint(4) default NULL,
  KEY `fk_release_id` (`release_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `match_method`
--

DROP TABLE IF EXISTS `match_method`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `match_method` (
  `id` int(11) NOT NULL,
  `name` varchar(32) default NULL,
  `notes` varchar(256) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `platform`
--

DROP TABLE IF EXISTS `platform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `platform` (
  `id` int(11) NOT NULL,
  `name` varchar(32) default NULL,
  `maker` varchar(32) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `probeset`
--

DROP TABLE IF EXISTS `probeset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `probeset` (
  `id` int(11) NOT NULL,
  `platform_id` int(11) default NULL,
  `name` varchar(64) default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_platform_id` (`platform_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `probeset2gene`
--

DROP TABLE IF EXISTS `probeset2gene`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `probeset2gene` (
  `probeset_id` int(11) default NULL,
  `gene_id` int(11) default NULL,
  `match_method_id` int(11) default NULL,
  `probeset_name` varchar(64) default NULL,
  `gene_name` varchar(32) default NULL,
  KEY `fk_probeset_id` (`probeset_id`),
  KEY `fk_gene_id` (`gene_id`),
  KEY `fk_match_method_id` (`match_method_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `release`
--

DROP TABLE IF EXISTS `release`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `release` (
  `id` int(11) NOT NULL,
  `platform_id` int(11) default NULL,
  `name` varchar(16) default NULL,
  `descr` varchar(256) default NULL,
  `array_order` blob,
  `probeset_order` blob,
  PRIMARY KEY  (`id`),
  KEY `fk_platform_id` (`platform_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `species`
--

DROP TABLE IF EXISTS `species`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `species` (
  `id` int(11) NOT NULL,
  `genus` varchar(64) default NULL,
  `species` varchar(64) default NULL,
  `variety` varchar(64) default NULL,
  `tax_id` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-03-18 15:54:35
