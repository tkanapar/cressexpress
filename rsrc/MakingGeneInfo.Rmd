Creating data file to populate the CressExpress gene_info table with data from the Arabidopsis TAIR10 release
========================================================

This R Markdown document describes creating a data file suitqbled for loading the gene_info table in the CressExpress database. 

The table has the following columns:

* gene_id - foreign key to gene table
* symbol - gene sybol
* description - descriptive text about a gene

To connect to the database, we'll use RMySQL library:

```{r}
# see http://stackoverflow.com/questions/12291418/how-can-i-make-r-read-my-environmental-variables
# for how to get your system environmental variables
# to be passed into R
DB=Sys.getenv('DB')
DBPASS=Sys.getenv('DBPASS')
DBUSER=Sys.getenv('DBUSER')
DBHOST=Sys.getenv('DBHOST')
# see http://www.r-bloggers.com/accessing-mysql-through-r/
# for tutorial on accessing MySQL database from
# inside R
library(RMySQL)
mydb=dbConnect(MySQL(),user=DBUSER,password=DBPASS,
               dbname=DB,host=DBHOST)
sql='select * from gene'
rs=dbSendQuery(mydb,sql)
data=fetch(rs,-1)
```

We retrieved `r nrow(data)` rows from the gene table.

Now let's retrieve the Arabidopsis genome annotations:

```{r}
u='http://www.igbquickload.org/quickload/A_thaliana_Jun_2009/TAIR10.bed.gz'
f='TAIR10.bed.gz'
if (!file.exists(f)) {
  download.file(u,f,'internal')
}
annots=read.delim(f,sep='\t',header=F,as.is=T)[,c(4,13,14)]
names(annots)=c('tx','symbol','description')
locus=sapply(strsplit(annots$tx,'\\.'),function(x){x[[1]]})
annots$locus=locus
o=order(annots$tx)
annots=annots[o,]
v=duplicated(annots$locus)
annots=annots[!v,]
```

Now we have two data frames:

* data from the gene table (`r nrow(data)`)
* gene symbol and description for every Arabidopsis gene (`r nrow(annots)` rows)

Merge them to make gene_info table:

```{r}
gene_info=merge(data,annots,by.x='name',by.y='locus')
gene_info=gene_info[,c('id','symbol','description')]
```

Write a file:

```{r}
f='gene_info.tsv'
write.table(gene_info,f,sep='\t',row.names=F,
            col.names=F,quote=F)
```

And that's it!

